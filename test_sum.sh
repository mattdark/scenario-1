#!/bin/sh

# Hm ... something is fishy with the math around here!

a=2
b=1
val=`expr $a + $b` # Try making this an addition operation

if [ "$val" -eq 3 ]
then
  echo "Ta-da!"
else
  echo "Uh-oh ... Something is wrong!"
  exit 1
fi;
