# 👋 Hello

Thank you for taking out time to be a part of this research activity.

Below are the tasks they you are required to perform as a part of this session:

## 𝍌 Task 1
In our scenario today, you are a software developer on a large team that uses a mono-repo for their projects. There is a CI/CD pipeline set-up to streamline the workflow of the developers and contributors.

You have been assigned the task to work on a set of features, and push the changes from your the feature branches to the target branch. You have to ensure your code passes a set of code quality checks when checked against the target branch before integration. The feature branch is already made available in the repository to help you kick-off the task.

[Link to documentation](https://docs.gitlab.com/ee/ci/introduction/index.html)


## 𝍌 Task 2
Make changes to one of the files in the repository and make sure a pipeline runs before the change is merged into the target branch. Observe the performance and health of the running pipeline. if there’s any abnormality in the behaviour, figure out what is causing it.


-------

🙏

